#ifndef SIERPINSKI_COORD_HH
# define SIERPINSKI_COORD_HH

# include <cassert>
# include <array>
# include <initializer_list>

# include "auto-ref.hh"

namespace misc
{
  template <uint size_,
            typename Type = float>
  class coord
  {
  public:
    class iterator {
      using type_t = Type;
      using type_ptr = typename misc::auto_ref<type_t>::type_t;
      using coord_t = misc::coord<size_, type_t>;
      using coord_ptr = coord_t&;
    public:
      iterator(coord_ptr self, uint i = 0)
        : s_(self)
        , i_(i) {
      }
      void operator++() {
        ++i_;
      }
      type_t& operator*() {
        return s_[i_];
      }
      bool operator !=(const iterator& r) const {
        return i_ != r.i_;
      }
    private:
      coord_ptr s_;
      uint i_;
    };
  public:
    using type_t = Type;
    using type_ptr = typename misc::auto_ref<type_t>::type_t;
    using vect_t = std::array<type_t, size_>;
    using vect_ptr = vect_t&;
    using self_t = coord<size_, type_t>;
    using self_ptr = self_t&;

  public:
    coord(const std::initializer_list<type_t>& x) {
      uint s = 0;
      for (auto i: x) {
        v_[s] = i;
        assert(s < size_);
        ++s;
      }
      assert(s == size_);
    }
    coord() {
      for(auto& i: v_)
        i = type_t();
    }
  public:
    iterator begin() {
      return iterator(*this);
    }
    iterator end() {
      return iterator(*this, size_);
    }

  public:
    void operator*(const type_ptr fx) {
      for (auto& v: v_)
        v *= fx;
    }
    void operator/(const type_ptr fx) {
      for (auto& v: v_)
        v /= fx;
    }
    void operator+(const type_ptr sc) {
      for (auto& v: v_)
        v += sc;
    }
    void operator-(const type_ptr sc) {
      for (auto& v: v_)
        v -= sc;
    }

    const self_ptr operator+=(const self_ptr r) {
      for(uint i = 0; i < v_.dim(); ++i)
        v_[i] += r.v_[i];
    }
    const self_ptr operator-=(const self_ptr r) {
      for(uint i = 0; i < v_.dim(); ++i)
        v_[i] -= r.v_[i];
    }
    const self_ptr operator*=(const self_ptr r) {
      for(uint i = 0; i < v_.dim(); ++i)
        v_[i] *= r.v_[i];
    }
    const self_ptr operator/=(const self_ptr r) {
      for(uint i = 0; i < v_.dim(); ++i)
        v_[i] /= r.v_[i];
    }

  public:
    const type_ptr operator[](const uint x) const {
      assert(x < size_);
      return v_[x];
    }
    type_t& operator[](const uint x) {
      assert(x < size_);
      return v_[x];
    }
    constexpr uint dim() {
      return size_;
    }
    const type_t* data() const {
      return v_.data();
    }

  private:
    vect_t v_;
  };

} // namespace misc

#endif // !SIERPINSKI_COORD_HH
