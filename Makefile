all: main

main: main.cc
	g++ -W -Wall -Wextra -std=c++11 -omain main.cc

main.cc: n-matrix.hh coord.hh
n-matrix.hh: typedef.hh
clean:
	rm -f main
