#include <iostream>
#include <fstream>
#include <cstdlib>

#include "coord.hh"
#include "n-matrix.hh"

double drandom(double max)
{
  // Generate a random number in [0, max[.
  return (random() / (double) RAND_MAX) * max;
}

int main()
{
  // Size of the output screen.
  const uint x = 1024;
  const uint y = 1024;

  using fl_t = double;
  using coord_t = misc::coord<2, fl_t>;
  using matrix_t = misc::nmatrix<bool, x, y>;
  using vector_t = std::vector<coord_t>;

  // Initialize variables
  srandom(0);

  matrix_t m;
  for (uint i = 0; i < x; ++i)
    for (uint j = 0; j < x; ++j)
      m[i][j] = false;

  coord_t iter { drandom(x), drandom(y) };

  vector_t v;
  v.push_back(coord_t{ 0, 0});
  v.push_back(coord_t{ x, 0});
  v.push_back(coord_t{ 0, y});

  uint size = v.size();

  // Iteration
  for (uint i = 0; i < 10000000; i++)
    {
      auto& point = v[random() % size];
      for (uint j = 0; j < iter.dim(); ++j)
        iter[j] = (iter[j] + point[j]) / 2;
      m[(uint) iter[0]][(uint) iter[1]] = true;
    }

  // Print the matrix in pgm format.
  std::ofstream ofs("foo.pgm");
  ofs << "P5" << std::endl << x << '\t' << x << '\t' << 255 << std::endl;
  for (uint i = 0; i < x; ++i)
    for (uint j = 0; j < x; ++j)
      if (m[i][j])
        ofs << ' ';
      else
        ofs << '~';
}
