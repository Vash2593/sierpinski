#ifndef SIERPINSKI_TYPEDEF_HH
# define SIERPINSKI_TYPEDEF_HH

using uchar = unsigned char;
using uint = unsigned int;
using ulong = unsigned long;
using ulonglong = unsigned long long;

#endif // !SIERPINSKI_TYPEDEF_HH
