#ifndef SIERPINSKI_N_MATRIX_HH
# define SIERPINSKI_N_MATRIX_HH

# include <array>
# include <cassert>
# include <vector>

# include "typedef.hh"
# include "auto-ref.hh"
# include "coord.hh"

namespace misc
{

  template <typename Type, uint... size>
  class nmatrix;

  template <typename Type, uint size_>
  class nmatrix<Type, size_>
  {
  public:
    class iterator
    {
    public:
      using type_t = Type;
      using type_ptr = typename misc::auto_ref<type_t>::type_t;
      using nmatrix_t = nmatrix<type_t, size_>;
      using nmatrix_ptr = nmatrix_t&;
    public:
      iterator(nmatrix_ptr self, uint init = 0)
          : s_(self)
          , i_(init) {
      }
      type_t& operator*() {
        return s_[i_];
      }
      uint coord() const {
        return i_;
      }
      void operator++() {
        ++i_;
      }
      bool operator !=(const iterator& r) const {
        return i_ != r.i_;
      }
    private:
      nmatrix_ptr s_;
      uint i_;
    };
  public:
    using type_t = Type;
    using type_ptr = typename misc::auto_ref<type_t>::type_t;
    using self_t = nmatrix<Type, size_>;
    using self_ptr = self_t&;
    using vector_t = std::array<type_t, size_>;
    using vector_ptr = vector_t&;
    using coord_t = std::array<uint, 1>;
    using coord_ptr = coord_t&;
  public:
    nmatrix() {
    }
  public:
    const type_ptr operator[](const uint x) const {
      assert(x < size_);
      return v_[x];
    }
    type_t& operator[](const uint x) {
      assert(x < size_);
      return v_[x];
    }
    const type_ptr operator[](const coord_ptr c) const {
      assert(c.size() == 1);
      return v_[c[0]];
    }
    type_t& operator[](const coord_ptr c) {
      assert(c.size() == 1);
      return v_[c[0]];
    }
  public: // Iterator
    iterator begin() {
      return iterator(*this);
    }
    iterator end() {
      return iterator(*this, size_);
    }
  public: // Dim
    const coord_t dim() const {
      return { size_ };
    }
    template <uint s>
    static
    void dim(misc::coord<s, uint>& res, uint i) {
      assert(i < s);
      res[i] = my_dim();
    }
    constexpr static uint my_dim() {
      return size_;
    }
  public:
    const type_ptr operator[](const uint *c) const {
      return v_[c[0]];
    }
    type_t& operator[](const uint *c) {
      return v_[c[0]];
    }
  public:
    vector_t v_;
  };

  template <typename Type, uint size_, uint... size>
  class nmatrix<Type, size_, size...>
  {
  public:
    class iterator
    {
    public:
      using type_t = Type;
      using type_ptr = typename misc::auto_ref<type_t>::type_t;
      using nmatrix_t = nmatrix<type_t, size_, size...>;
      using nmatrix_ptr = nmatrix_t&;
      using coord_t = misc::coord<sizeof...(size) + 1, uint>;
      using coord_ptr = coord_t&;
    public:
      iterator(nmatrix_ptr self, bool e = false)
        : s_(self)
        , e_(self.dim()) {
        for (uint& i: i_)
          i = 0;
        if (e)
          i_[0] = e_[0];

      }
      void operator++() {
        for (uint i = sizeof...(size) + 1; i > 0;)
          {
            --i;
            ++i_[i];
            if (i_[i] < e_[i])
                break;
            if (i)
              i_[i] = 0;
          }
      }
      type_ptr operator*() {
        return s_[i_];
      }
      coord_t& coord() {
        return i_;
      }
      bool operator!=(const iterator& r) const {
        for (uint i = sizeof...(size) + 1; i > 0;)
          {
            --i;
            if (i_[i] != r.i_[i])
              return true;
          }
        return false;
      }
    private:
      nmatrix_ptr s_;
      coord_t i_;
      coord_t e_;
    };
  public:
    using type_t = Type;
    using type_ptr = typename misc::auto_ref<type_t>::type_t;
    using self_t = nmatrix<type_t, size_, size...>;
    using self_ptr = self_t&;
    using smatrix_t = nmatrix<type_t, size...>;
    using smatrix_ptr = smatrix_t&;
    using vector_t = std::array<smatrix_t, size_>;
    using vector_ptr = vector_t&;
    using coord_t = misc::coord<sizeof...(size) + 1, uint>;
    using coord_ptr = coord_t&;
  public:
    nmatrix() {
    }
  public:
    const smatrix_ptr operator[](const uint x) const {
      assert(x < size_);
      return v_[x];
    }
    smatrix_ptr operator[](const uint x) {
      assert(x < size_);
      return v_[x];
    }
    const type_ptr operator[](const coord_ptr c) const {
      return operator[](c.data());
    }
    type_t& operator[](coord_ptr c) {
      return operator[](c.data());
    }
  public:
    iterator begin() {
      return iterator(*this);
    }
    iterator end() {
      return iterator(*this, true);
    }
    coord_t dim() const {
      coord_t res;
      res[0] = my_dim();
      smatrix_t::template dim<sizeof...(size) + 1>(res, 1);
      return res;
    }
    template <uint s>
    static
    void dim(misc::coord<s, uint>& res, uint i) {
      assert(i < s);
      res[i] = my_dim();
      smatrix_t::template dim<s>(res, i + 1);
    }
    constexpr static uint my_dim() {
      return size_;
    }
  private:
    const type_ptr operator[](const uint* c) const {
      return v_[c[0]][c + 1];
    }
    type_t& operator[](const uint* c) {
      return v_[c[0]][c + 1];
    }
  private:
    vector_t v_;
  };

} // namespace misc

#endif // !SIERPINSKI_N_MATRIX_HH
