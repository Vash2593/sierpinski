#ifndef SIERPINSKI_AUTO_REF_HH
# define SIERPINSKI_AUTO_REF_HH

namespace misc
{
  template <typename T>
  struct auto_ref;

  // Specialization
  template <>
  struct auto_ref<int>
  {
    using type_t = int;
  };

  template <>
  struct auto_ref<char>
  {
    using type_t = char;
  };

  template <>
  struct auto_ref<unsigned>
  {
    using type_t = unsigned;
  };

  template <>
  struct auto_ref<unsigned char>
  {
    using type_t = unsigned char;
  };

  template <>
  struct auto_ref<long>
  {
    using type_t = long;
  };

  template <>
  struct auto_ref<unsigned long>
  {
    using type_t = unsigned long;
  };

  template <>
  struct auto_ref<long long>
  {
    using type_t = long long;
  };

  template <>
  struct auto_ref<unsigned long long>
  {
    using type_t = unsigned long long;
  };

  template <>
  struct auto_ref<short>
  {
    using type_t = short;
  };

  template <>
  struct auto_ref<unsigned short>
  {
    using type_t = unsigned short;
  };

  template <>
  struct auto_ref<bool>
  {
    using type_t = bool;
  };

  template <>
  struct auto_ref<double>
  {
    using type_t = double;
  };

  template <>
  struct auto_ref<float>
  {
    using type_t = float;
  };

  template <typename T>
  struct auto_ref<T*>
  {
    using type_t = T*;
  };

  // General case
  template <typename T>
  struct auto_ref
  {
    using type_t = T&;
  };

} // namespace misc

#endif // !SIERPINSKI_AUTO_REF_HH
